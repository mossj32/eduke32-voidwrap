// Voidwrap - A steam API wrapper for use by Voidpoint.

/**
 * The MIT License
 * Copyright (c) 2019 Jordon Moss
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#define VOIDWRAP_ISEXPORTING
#include "voidwrap.h"
#include "compat.h" // from source/build/include

// Info
CSteamID SteamID;
uint64_t SteamID64;
uint64_t AppID;
int32_t NumControllerHandles;

SteamScreenshotHandler* ScreenHandler;
ControllerHandle_t* ControllerHandles;

VW_CALLBACK_NOPARAM Callback_ScreenshotRequested;
//VW_CALLBACK_INT32 Callback_ScreenshotReady;
VW_CALLBACK_CHARPTR Callback_PrintDebug;

// ----------------------------------
// ---- Steam API Main Functions ----
// ----------------------------------

VOIDWRAP_API bool Voidwrap_InitSteam()
{
	if (SteamAPI_Init())
	{
		SteamUtils()->SetWarningMessageHook(&SteamAPIWarningMessageHook);
		SteamScreenshots()->HookScreenshots(true);
		SteamID = SteamUser()->GetSteamID();
		SteamID64 = SteamID.ConvertToUint64();
		AppID = SteamUtils()->GetAppID();
		PrintDebug("AppID is %llu", AppID);

		if (SteamScreenshots()->IsScreenshotsHooked()) { PrintDebug("Screenshots hooked."); }
		if (SteamUtils()->IsOverlayEnabled()) { PrintDebug("Overlay is enabled."); }
		if (SteamController()->Init()) { PrintDebug("Controller API init succeeded."); }

		ControllerHandles = new ControllerHandle_t[STEAM_CONTROLLER_MAX_COUNT];
		ScreenHandler = new SteamScreenshotHandler();

		SteamAPI_RunCallbacks();

		return true;
	}
	else
	{
		SteamAPI_Shutdown();
		return false;
	}
}

VOIDWRAP_API void Voidwrap_ShutdownSteam()
{
	SteamAPI_Shutdown();
}

VOIDWRAP_API void Voidwrap_RunCallbacks()
{
	SteamAPI_RunCallbacks();
}

// ---- End Steam API Main Functions ----

// --------------------------
// ---- Callback Setters ----
// --------------------------
// Preferably, I'd like to have one, universal callback registration function, but this will do for now.

VOIDWRAP_API void Voidwrap_SetCallback_ScreenshotRequested(VW_CALLBACK_NOPARAM function)
{
	Callback_ScreenshotRequested = function;
}

#if 0
VOIDWRAP_API void Voidwrap_SetCallback_ScreenshotReady(VW_CALLBACK_INT32 function)
{
	Callback_ScreenshotReady = function;
}
#endif

VOIDWRAP_API void Voidwrap_SetCallback_PrintDebug(VW_CALLBACK_CHARPTR function)
{
	Callback_PrintDebug = function;
}

// ---- End Callback Setters ----

// ------------------------
// ---- Screenshot API ----
// ------------------------

VOIDWRAP_API bool Voidwrap_SendSteamScreenshot(char* filepath, int32_t width, int32_t height)
{
	if (INVALID_SCREENSHOT_HANDLE == SteamScreenshots()->AddScreenshotToLibrary(filepath, NULL, width, height))
	{
		return false;
	}

	return true;
}

void SteamScreenshotHandler::screenshotRequested(ScreenshotRequested_t* pCallback)
{
	if (Callback_ScreenshotRequested != nullptr)
	{
		PrintDebug("SteamScreenshotHandler::screenshotRequested executed.");
		Callback_ScreenshotRequested();
	}
}

void SteamScreenshotHandler::screenshotReady(ScreenshotReady_t* pCallback)
{
	PrintDebug("SteamScreenshotHandler::screenshotReady executed. Result: %d", pCallback->m_eResult);

#if 0
	if (Callback_ScreenshotReady != nullptr)
	{
		Callback_ScreenshotReady(pCallback->m_eResult);
	}
#endif
}

// ---- End Screenshot API

// ------------------------
// ---- Controller API ----
// ------------------------

VOIDWRAP_API int32_t Voidwrap_GetConnectedControllers()
{
	SteamController()->RunFrame(); // Just in case there's any controller events queued up.
	NumControllerHandles = SteamController()->GetConnectedControllers(ControllerHandles);
	return NumControllerHandles;
}

// ---- End Controller API ----

// -------------------------
// ---- Debugging Tools ----
// -------------------------

void PrintDebug(const char* fmt, ...)
{
	static char tmpstr[8192];
	va_list va;

	va_start(va, fmt);
	Bvsnprintf(tmpstr, sizeof(tmpstr), fmt, va);
	va_end(va);

	if (Callback_PrintDebug != nullptr)
	{
		Callback_PrintDebug(tmpstr);
	}
}

void SteamAPIWarningMessageHook(int nSeverity, const char* pchDebugText)
{
	PrintDebug(pchDebugText);
}

// ---- End Debugging Tools ----