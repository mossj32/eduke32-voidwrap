#ifndef VOIDWRAP_H
	#define VOIDWRAP_H

	#include <inttypes.h>
	#include <stdbool.h>

	// Cross-platform Definitions & Includes
	#ifdef _WIN32
		#include <windows.h>
		#define VW_LIBHANDLE HINSTANCE
		#define Voidwrap_LoadLibrary(lib) LoadLibrary(lib)
		#define Voidwrap_GetSymbol(lib_handle, symbol) GetProcAddress(lib_handle, symbol)
	#else
		#include <dlfcn.h>
		#define VOIDWRAP_API
		#define VW_LIBHANDLE void*
		#define Voidwrap_LoadLibrary(lib) dlopen(lib, RTLD_NOW|RTLD_GLOBAL)
		#define Voidwrap_GetSymbol(lib_handle, symbol) dlsym(lib_handle, symbol)
	#endif

	#ifdef VOIDWRAP_ISEXPORTING
		// Includes
		#include "steam_api.h"

		// Definitions
		#ifdef _WIN32
			#define VOIDWRAP_API __declspec(dllexport)
		#endif

		// Functions
		void PrintDebug(const char* fmt, ...);
		void SteamAPIWarningMessageHook(int nSeverity, const char* pchDebugText);

		// Classes
		class SteamScreenshotHandler
		{
			private:
				STEAM_CALLBACK(SteamScreenshotHandler, screenshotRequested, ScreenshotRequested_t);
				STEAM_CALLBACK(SteamScreenshotHandler, screenshotReady, ScreenshotReady_t);
		};
	#else
		// Definitions
		#ifdef _WIN32
			#define VOIDWRAP_API __declspec(dllimport)
		#endif
	#endif

	// Typedefs
	// -- Callback types --
	typedef void (*VW_CALLBACK_NOPARAM)();
	typedef void (*VW_CALLBACK_INT32)(int32_t);
	typedef void (*VW_CALLBACK_CHARPTR)(const char*);

	// -- Callback setup function types --
	typedef void(*VW_SETCALLBACK_NOPARAM)(VW_CALLBACK_NOPARAM function);
	typedef void(*VW_SETCALLBACK_INT32)(VW_CALLBACK_INT32 function);
	typedef void(*VW_SETCALLBACK_CHARPTR)(VW_CALLBACK_CHARPTR function);

	// -- Function types --
	typedef bool(*VW_BOOL)();
	typedef bool(*VW_BOOL_SCREENSHOT)(char* filepath, int32_t width, int32_t height);
	typedef void(*VW_VOID)();
	typedef int32_t(*VW_INT32)();
	
	// Exports/Imports
	// NOTE: Use VOIDWRAP_RUNTIMELINK define in your app
	// before including header if not using load-time linking.
	#ifndef VOIDWRAP_RUNTIMELINK
	extern "C"
	{
		VOIDWRAP_API bool Voidwrap_InitSteam();
		VOIDWRAP_API void Voidwrap_ShutdownSteam();
		VOIDWRAP_API void Voidwrap_RunCallbacks();

		// Screenshot API
		VOIDWRAP_API bool Voidwrap_SendSteamScreenshot(char* filepath, int32_t width, int32_t height);

		// Controller API
		VOIDWRAP_API int32_t Voidwrap_GetConnectedControllers();

		// Callback Setters
		VOIDWRAP_API void Voidwrap_SetCallback_ScreenshotRequested(VW_CALLBACK_NOPARAM function);
		//VOIDWRAP_API void Voidwrap_SetCallback_ScreenshotReady(VW_CALLBACK_INT32 function);
		VOIDWRAP_API void Voidwrap_SetCallback_PrintDebug(VW_CALLBACK_CHARPTR function);
	}
	#else
	// Keep in sync with above section.
	VW_BOOL Voidwrap_InitSteam;
	VW_VOID Voidwrap_ShutdownSteam;
	VW_VOID Voidwrap_RunCallbacks;

	// Screenshot API
	VW_BOOL_SCREENSHOT Voidwrap_SendSteamScreenshot;

	// Controller API
	VW_INT32 Voidwrap_GetConnectedControllers;

	// Callback Setters
	VW_SETCALLBACK_NOPARAM Voidwrap_SetCallback_ScreenshotRequested;
	//VW_SETCALLBACK_INT32 Voidwrap_SetCallback_ScreenshotReady;
	VW_SETCALLBACK_CHARPTR Voidwrap_SetCallback_PrintDebug;
	#endif
#endif
