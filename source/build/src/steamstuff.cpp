#define VOIDWRAP_RUNTIMELINK
#include "voidwrap.h"
#include "build.h"

bool steamworks_enabled;
static VW_LIBHANDLE wrapper_handle = NULL;

#ifdef _WIN32
    #ifdef _WIN64
    const char* wrapper_lib = "voidwrap_x64.dll";
    #else
    const char* wrapper_lib = "voidwrap_x86.dll";
    #endif
#else // Linux (and Mac?)
    const char* wrapper_lib = "libvoidwrap.so.0";
#endif

void steam_callback_screenshotrequested()
{
    initprintf("Voidwrap: Preparing steam screenshot!\n");
    videoCaptureScreen("steam0000.png", 0);
}

#if 0
void steam_callback_screenshotready(int32_t result)
{
    initprintf("Voidwrap: Steam screenshot ready! - Result: %d\n", result);
}
#endif

void steam_callback_printdebug(const char* str)
{
    initprintf("[DEBUG](%s): %s\n", wrapper_lib, str);
}

void steam_init(void)
{
    wrapper_handle = Voidwrap_LoadLibrary(wrapper_lib);
    if (wrapper_handle == NULL)
    {
        #ifdef _WIN32
        initprintf("Voidwrap: %s missing or load failed.\n", wrapper_lib);
        #else
        initprintf("Voidwrap: %s dlopen error: %s\n", wrapper_lib, dlerror());
        #endif
        return;
    }

    initprintf("Voidwrap: %s found!\n", wrapper_lib);
    Voidwrap_InitSteam = (VW_BOOL)Voidwrap_GetSymbol(wrapper_handle, "Voidwrap_InitSteam");
    Voidwrap_ShutdownSteam = (VW_VOID)Voidwrap_GetSymbol(wrapper_handle, "Voidwrap_ShutdownSteam");
    Voidwrap_RunCallbacks = (VW_VOID)Voidwrap_GetSymbol(wrapper_handle, "Voidwrap_RunCallbacks");
    Voidwrap_SendSteamScreenshot = (VW_BOOL_SCREENSHOT)Voidwrap_GetSymbol(wrapper_handle, "Voidwrap_SendSteamScreenshot");
    Voidwrap_SetCallback_ScreenshotRequested = (VW_SETCALLBACK_NOPARAM)Voidwrap_GetSymbol(wrapper_handle, "Voidwrap_SetCallback_ScreenshotRequested");
    //Voidwrap_SetCallback_ScreenshotReady = (VW_SETCALLBACK_INT32)Voidwrap_GetSymbol(wrapper_handle, "Voidwrap_SetCallback_ScreenshotReady");
    Voidwrap_SetCallback_PrintDebug = (VW_SETCALLBACK_CHARPTR)Voidwrap_GetSymbol(wrapper_handle, "Voidwrap_SetCallback_PrintDebug");

    Voidwrap_SetCallback_ScreenshotRequested(steam_callback_screenshotrequested);
    //Voidwrap_SetCallback_ScreenshotReady(steam_callback_screenshotready);
    Voidwrap_SetCallback_PrintDebug(steam_callback_printdebug);

    if (Voidwrap_InitSteam == NULL || Voidwrap_RunCallbacks == NULL)
    {
        initprintf("Voidwrap: getproc_ failure.\n");
        return;
    }

    if (!Voidwrap_InitSteam())
    {
        initprintf("Voidwrap: Steamworks init failure.\n");
        return;
    }

    initprintf("Voidwrap: Steamworks init success!\n");
    steamworks_enabled = true;
}

// Should be called during engine shutdown.
void steam_shutdown()
{
    if (!steamworks_enabled)
        return;

    Voidwrap_ShutdownSteam();
}

void steam_runcallbacks()
{
    if (!steamworks_enabled)
        return;

    Voidwrap_RunCallbacks();
}

void steam_sendscreenshot(char* filename)
{
    if (!steamworks_enabled)
        return;

    char fullpath[BMAX_PATH];
    buildvfs_getcwd(fullpath, sizeof(fullpath));
    Bstrcat(fullpath, "/");
    Bstrcat(fullpath, filename);
    OSD_Printf("Voidwrap: Steam screenshot full path: %s\n", fullpath);

    Voidwrap_SendSteamScreenshot(fullpath, xdim, ydim);
}
