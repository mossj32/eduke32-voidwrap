#ifndef STEAMSTUFF_H_
#define STEAMSTUFF_H_

#include "compat.h"

extern bool steamworks_enabled;

void steam_init(void);
void steam_runcallbacks(void);
void steam_sendscreenshot(char* filename);
void steam_shutdown(void);

#endif